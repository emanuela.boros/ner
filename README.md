#### Run the code

BERT models need to be dowloaded (with the exception of CamemBERT)

Training:

```
TOKENIZERS_PARALLELISM=false CUDA_VISIBLE_DEVICES=1 python main.py \
 --directory ../DATA/semeval_mutilingual_ner/training_data/MULTI_Multilingual/ \
 --pre_trained_model xlm-roberta-large \
 --train_dataset ../DATA/semevamutilingual_ner/training_data/MULTI_Multilingual/multi_train.conll \
 --test_dataset ../DATA/semeval_mutilingual_ner/training_data/MULTI_Multilingual/multi_test.conll \
 --dev_dataset ../DATA/semeval_mutilingual_ner/training_data/MULTI_Multilingual/multi_dev.conll \
 --batch_size 4  --do_train  --no_cpu 5 --language english --model stacked --num_layers 2


```
##### Dataset Annotation


```
TOKEN	NE-COARSE-LIT	NE-COARSE-METO	NE-FINE-LIT	NE-FINE-METO	NE-FINE-COMP	NE-NESTED	NEL-LIT	NEL-METO	MISC
Wienstrasse	I-LOC	O	O	O	O	O	null	O	SpaceAfter

```

#### Requirements
```
pip install -r requirements.txt
```
